/// Несколько оптимизаций перебора делителей
pub fn calc(n: u64) -> u64 {
    if n < 2 { return 0; }

    let mut count = 1;

    for i in 2..(n+1) {
        if i % 2 == 0 {
            continue;
        }

        if is_prime(i) {
            count += 1;
        }
    }

    count
}

fn is_prime(n: u64) -> bool {
    let top_bound: u64 = (n as f64).sqrt().floor() as u64 + 1;

    for i in 2..top_bound {
        if n % i == 0 {
            return false;
        }
    }

    true
}