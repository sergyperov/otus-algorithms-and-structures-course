mod unoptimized;
mod optimized;
mod eratosthenes_sieve;
mod eratosthenes_sieve_bitwise;
mod eratosthenes_sieve_fast;

pub fn unoptimized(input: String) -> String {
    unoptimized::calc(input.trim().parse().unwrap_or(0)).to_string()
}

pub fn optimized(input: String) -> String {
    optimized::calc(input.trim().parse().unwrap_or(0)).to_string()
}

pub fn eratosthenes_sieve(input: String) -> String {
    eratosthenes_sieve::calc(input.trim().parse().unwrap_or(0)).to_string()
}

pub fn eratosthenes_sieve_bitwise(input: String) -> String {
    eratosthenes_sieve_bitwise::calc(input.trim().parse().unwrap_or(0)).to_string()
}

pub fn eratosthenes_sieve_fast(input: String) -> String {
    eratosthenes_sieve_fast::calc(input.trim().parse().unwrap_or(0)).to_string()
}