/// Через перебор делителей
pub fn calc(n: u64) -> u64 {
    let mut count = 0;

    for i in 2..(n+1) {
        if is_prime(i) {
            count += 1;
        }
    }

    count
}

fn is_prime(n: u64) -> bool {
    let mut count: u64 = 0;

    for i in 1..n+1 {
        if n % i == 0 {
            count += 1;
        }
    }

    count == 2
}