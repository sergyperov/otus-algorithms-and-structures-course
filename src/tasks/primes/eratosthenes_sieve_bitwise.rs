/// Решето Эратосфена с оптимизацией памяти: битовая матрица, по 32 значения в одном int
pub fn calc(n: u64) -> u64 {
    if n < 2 {
        return 0;
    }

    let sieve_size: usize = ((n as f32) / 128f32).ceil() as usize;
    let mut sieve: Vec<u64> = vec![0u64; sieve_size];
    let mut count: u64 = 1;

    let mut i: u64 = 3;
    while i <= n {
        if is_prime(&sieve, i) {
            count += 1;
            let mut j: u64 = i * i;
            let k: u64 = i << 1;
            while j <= n {
                make_composite(sieve.as_mut(), j);
                j += k;
            }
        }
        i += 2;
    }

    count
}

fn is_prime(sieve: &Vec<u64>, n: u64) -> bool {
    let index: usize = (n / 128) as usize;
    (sieve[index] & (1 << ((n >> 1) & 63))) == 0
}

fn make_composite(sieve: &mut Vec<u64>, n: u64) {
    let index: usize = (n / 128) as usize;
    sieve[index] |= 1 << ((n >> 1) & 63);
}