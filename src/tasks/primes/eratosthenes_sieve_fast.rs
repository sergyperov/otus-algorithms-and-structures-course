/// Решето Эратосфена с оптимизацией памяти: битовая матрица, по 32 значения в одном int
pub fn calc(n: u32) -> u32 {
    if n < 2 {
        return 0;
    }

    // целочисленный массив, индексируемый от 2 до n, заполненный нулями
    let mut lp: Vec<u32> = vec![0u32; (n + 1) as usize];
    // целочисленный массив, поначалу пустой
    let mut pr: Vec<u32> = Vec::new();

    for i in 2..n+1 {
        if lp[i as usize] == 0 {
            lp[i as usize] = i;
            pr.push(i);
        }

        for p in pr.iter() {
            if *p > lp[i as usize] || ((*p) * i) > n {
                break;
            }

            lp[(p * i) as usize] = *p;
        }
    }

    pr.len() as u32
}