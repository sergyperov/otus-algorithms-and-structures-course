/// Решето Эратосфена со сложностью O(n log log n)
pub fn calc(n: u64) -> u64 {
    let mut sieve: Vec<bool> = vec![true; (n+1) as usize];
    let mut count: u64 = 0;

    let mut a: u64 = 2;
    while a <= n {
        // число помечено как не простое — пропускаем
        if sieve[a as usize] == false {
            a += 1;
            continue;
        }
        count += 1;

        let mut c: u64 = a;

        while c <= n {
            sieve[c as usize] = false;
            c += a;
        }
        a += 1;
    }

    count
}