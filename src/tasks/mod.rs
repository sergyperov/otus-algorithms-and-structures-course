// Это случжебный модуль для экспорта всех подмодулей (задач)

pub mod strings;
pub mod tickets;
pub mod harry_potter;
pub mod power;
pub mod fibonacci;
pub mod gcd;
pub mod primes;