use num_bigint::{BigUint};
use num_traits::identities::{Zero};

/// Алгоритм Евклида через остаток
pub fn calc(mut a: BigUint, mut b: BigUint) -> BigUint {
    while !a.is_zero() && !b.is_zero() {
        if a > b {
            a = a % &b;
        } else {
            b = b % &a;
        }
    }

    a + b
}