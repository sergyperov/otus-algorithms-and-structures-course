use num_bigint::{BigUint};
use num_traits::identities::{Zero};

mod bits;
mod euclid;
mod euclid_mod;

pub fn euclid(input: String) -> String {
    let (a, b) = parse_input(input);
    euclid::calc(a, b).to_string()
}

pub fn euclid_mod(input: String) -> String {
    let (a, b) = parse_input(input);
    euclid_mod::calc(a, b).to_string()
}

pub fn bits(input: String) -> String {
    let (a, b) = parse_input(input);
    bits::calc(a, b).to_string()
}

fn parse_input(input: String) -> (BigUint, BigUint) {
    let input_as_vec = input.split("\n").collect::<Vec<&str>>();
    let a_str: &str = input_as_vec[0].trim();
    let b_str: &str = input_as_vec[1].trim();

    let a: BigUint = BigUint::parse_bytes(a_str.as_bytes(), 10).unwrap_or(Zero::zero());
    let b: BigUint = BigUint::parse_bytes(b_str.as_bytes(), 10).unwrap_or(Zero::zero());

    (a, b)
}