use num_bigint::{BigUint};
use num_traits::identities::{Zero};

/// Алгоритм Стейнца через битовые операции
pub fn calc(mut a: BigUint, mut b: BigUint) -> BigUint {
    // выход из рекурсии
    if a == b {
        return a;
    }

    let is_a_even: bool = &a % BigUint::from(2u64) == Zero::zero();
    let is_b_even: bool = &b % BigUint::from(2u64) == Zero::zero();

    // алгоритм Стейнца с побитовым сдвигом
    if is_a_even && is_b_even {
        return BigUint::from(2u64) * calc(&a >> 1, &b >> 1);
    } else if is_a_even {
        return calc(&a >> 1, b);
    } else if is_b_even {
        return calc(a, &b >> 1);
    }

    // оптимизированный алгоритм Евклида
    return if a > b {
        calc((a - &b) >> 1, b)
    } else {
        calc((b - &a) >> 1, a)
    }
}