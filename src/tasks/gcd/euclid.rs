use num_bigint::{BigUint};

/// Алгоритм Евклида через вычитание
pub fn calc(mut a: BigUint, mut b: BigUint) -> BigUint {
    while a != b {
        if a > b {
            a = a - &b;
        } else {
            b = b - &a;
        }
    }

    a
}