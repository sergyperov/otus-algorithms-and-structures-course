pub fn string_length(input: String) -> String {
    input.trim().len().to_string()
}