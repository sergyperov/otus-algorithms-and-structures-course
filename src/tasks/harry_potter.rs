use std::fs;
use std::fs::File;
use std::io::prelude::*;

use ndarray::{Array2, Axis};
use ndarray::prelude::*;
use std::f32::consts::PI;

fn matrix_to_file(a: Array2<bool>, mut directory: String, filename: String) -> std::io::Result<()> {
    let mut owned_string: String = String::from(directory.clone());
    owned_string.push_str(&filename);

    fs::create_dir_all(directory)?;
    let mut file = File::create(owned_string)?;
    for i in 0..a.len_of(Axis(0)) {
        for j in 0..a.len_of(Axis(1)) {
            let symbol = if a[[i, j]] {b"# "} else {b". "};
            file.write(symbol)?;
        }
        file.write(b"\n")?;
    }
    Ok(())
}

pub fn create_squares() -> std::io::Result<()> {
    let dir = String::from("tasks_results/harry_potter/");
    create_square(|x, y| x < y, dir.clone(), String::from("01.txt"))?;
    create_square(|x, y| x == y, dir.clone(), String::from("02.txt"))?;
    create_square(|x, y| x == 24 - y, dir.clone(), String::from("03.txt"))?;
    create_square(|x, y| x + y < 30, dir.clone(), String::from("04.txt"))?;
    create_square(|x, y| x + 1 == (y+1) / 2 + (y+1) % 2, dir.clone(), String::from("05.txt"))?;
    // или просто x < 10 || y < 10
    create_square(|x, y| (x + y).abs() - (x - y).abs() < 20, dir.clone(), String::from("06.txt"))?;
    // или просто x > 15 && y > 15
    create_square(|x, y| (x + y).abs() - (x - y).abs() > 30, dir.clone(), String::from("07.txt"))?;
    create_square(|x, y| x * y == 0, dir.clone(), String::from("08.txt"))?;
    create_square(|x, y| (x - y).abs() > 10, dir.clone(), String::from("09.txt"))?;
    create_square(|x, y| (x - 1) * (y - 1) * (x - 23) * (y - 23) == 0, dir.clone(), String::from("11.txt"))?;
    create_square(|x, y| x.pow(2) + y.pow(2) <= 400, dir.clone(), String::from("12.txt"))?;
    create_square(|x, y| (24 - x).pow(2) + (24 - y).pow(2) >= 378, dir.clone(), String::from("14.txt"))?;
    create_square(|x, y| x * y * (x - 24) * (y - 24) == 0, dir.clone(), String::from("19.txt"))?;
    create_square(|x, y| (x + y) % 2 == 0, dir.clone(), String::from("20.txt"))?;
    create_square(|x, y| (x + y) % 3 == 0, dir.clone(), String::from("22.txt"))?;
    create_square(|x, y| (12 - x).abs() == (12 - y).abs(), dir.clone(), String::from("24.txt"))?;

    Ok(())
}

pub fn create_square(f: fn(i32, i32) -> bool, directory: String, filename: String) -> std::io::Result<()> {
    let mut arr: Array2<bool> = Array::from_elem((25, 25), false);
    for x in 0..25 {
        for y in 0..25 {
            arr[[x, y]] = f(x as i32, y as i32);
        }
    }

    matrix_to_file(arr, directory, filename)
}