/// Итеративное возведение в степень
/// Просто n раз умножаем число a на единицу
/// O(n)
pub fn calc(a: f64, n: u64) -> f64 {
    let mut res: f64 = 1.0;
    let mut counter: u64 = 0;
    while counter < n {
        res *= a;
        counter += 1;
    }

    res
}