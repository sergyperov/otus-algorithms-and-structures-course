mod iterative;
mod jumps;
mod optimized;

pub fn iterative(input: String) -> String {
    let (a, n) = parse_input(input);
    format_output(iterative::calc(a, n))
}

pub fn jumps(input: String) -> String {
    let (a, n) = parse_input(input);
    format_output(jumps::calc(a, n))
}

pub fn optimized(input: String) -> String {
    let (a, n) = parse_input(input);
    format_output(optimized::calc(a, n))
}

fn parse_input(input: String) -> (f64, u64) {
    let input_as_vec = input.split("\n").collect::<Vec<&str>>();
    let a: f64 = input_as_vec[0].trim().parse().unwrap_or(0.0);
    let n: u64 = input_as_vec[1].trim().parse().unwrap_or(0);

    (a, n)
}

fn format_output(res: f64) -> String {
    if res.fract() == 0.0 {
        // если результат целый, дописываем .0 в конец
        format!("{:.1}", res)
    } else {
        // точность 11 знаков после запятой, но лишние нули выводить не надо
        let y = (res * 100_000_000_000.0).round() / 100_000_000_000.0;
        format!("{}", y)
    }
}