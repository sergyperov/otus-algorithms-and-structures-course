/// Через двоичное разложение показателя степени
/// Раскладываем число n в двоичное преставление и перемножаем только степени, соответсвующие единичным рангам
/// O(log n)
pub fn calc(a: f64, n: u64) -> f64 {
    let mut res: f64 = 1.0;
    let mut power: f64 = a;
    let mut counter: u64 = n;

    while counter > 0 {
        if counter % 2 == 1 {
            res *= power;
        }

        counter /= 2;
        power *= power;
    }

    res
}