use std::string::ToString;
use std::cmp;

pub fn tickets(input: String) -> String {
    calculate_tickets(input.parse().unwrap_or(0)).to_string()
}

fn calculate_tickets(n: i8) -> u64 {
    let mut sum = 0;
    let mut cache: Vec<Vec<u64>> = vec![vec![0; (n*9 + 1) as usize]; 11];
    for i in 0..(n*9 + 1) {
        sum += d(n, i, &mut cache).pow(2);
    }

    sum
}

// d(n, k) — это количество n-значных фичел с сумой цифр k
// Для быстродействия алгоритма использует Динамическое программирование и memoization (кэширование результатов)
fn d(n: i8, k: i8, cache: &mut Vec<Vec<u64>>) -> u64 {
    if cache[n as usize][k as usize] != 0 {
        return cache[n as usize][k as usize];
    }
    let result =  if n == 1 {
        if k < 10 { 1 } else { 0 }
    } else {
        // рекурсивно находим текущее значение d(n, k)
        // d(n, k) равен сумма всех d(n - 1, i) где i = 0..min(k, 9)
        let mut sum: u64 = 0;
        for i in 0..cmp::min(k+1, 10) {
            sum += d(n - 1, k - i, cache);
        }

        sum
    };
    cache[n as usize][k as usize] = result;

    result
}