use num_bigint::{BigUint};

/// Через возведение матрицы в степень
pub fn calc(n: u64) -> BigUint {
    let res: [BigUint; 4] = [
        BigUint::from(1u64), BigUint::from(1u64),
        BigUint::from(1u64), BigUint::from(0u64)
    ];

    matrix_power(res, n)[2].clone()
}

/// Вспомогательная функция для возведения матрицы в степень
/// O(log n)
fn matrix_power(matrix: [BigUint; 4], n: u64) -> [BigUint; 4] {
    // изначально единичная матрица
    let mut res: [BigUint; 4] = [
        BigUint::from(1u64), BigUint::from(0u64),
        BigUint::from(0u64), BigUint::from(1u64)
    ];
    let mut power: [BigUint; 4] = matrix;
    let mut counter: u64 = n;

    while counter > 0 {
        if counter % 2 == 1 {
            res = multiply_matrix(res.clone(), power.clone());
        }

        counter /= 2;
        power = multiply_matrix(power.clone(), power.clone());
    }

    res
}

/// Умножение двух матриц
fn multiply_matrix(a: [BigUint; 4], b: [BigUint; 4]) -> [BigUint; 4] {
    [
        a[0].clone() * b[0].clone() + a[1].clone() * b[2].clone(),  a[0].clone() * b[1].clone() + a[1].clone() * b[3].clone(),
        a[2].clone() * b[0].clone() + a[3].clone() * b[2].clone(),  a[2].clone() * b[1].clone() + a[3].clone() * b[3].clone()
    ]
}