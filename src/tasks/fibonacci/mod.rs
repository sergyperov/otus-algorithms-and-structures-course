use num_bigint::{BigUint};

mod recursive;
mod iterative;
mod formula;
mod matrix;

pub fn recursive(input: String) -> String {
    let n: u64 = input.trim().parse().unwrap_or(0);
    let mut cache: Vec<BigUint> = vec![BigUint::from(0u64); (n+1) as usize];
    recursive::calc(n, &mut cache).to_string()
}

pub fn iterative(input: String) -> String {
    let n: u64 = input.trim().parse().unwrap_or(0);
    iterative::calc(n).to_string()
}

pub fn formula(input: String) -> String {
    let n: u64 = input.trim().parse().unwrap_or(0);
    formula::calc(n).to_string()
}

pub fn matrix(input: String) -> String {
    let n: u64 = input.trim().parse().unwrap_or(0);
    matrix::calc(n).to_string()
}