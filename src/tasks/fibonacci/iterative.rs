use num_bigint::{BigUint};

/// Через итерацию
pub fn calc(n: u64) -> BigUint {
    let mut a: BigUint = BigUint::from(0u64);
    let mut b: BigUint = BigUint::from(1u64);

    for _ in 2..n+1 {
        let c = a.clone() + b.clone();
        a = b.clone();
        b = c;
    }

    b
}