use num_bigint::{BigUint};

/// Через формулу золотого сечения
pub fn calc(n: u64) -> BigUint {
    let fi: f64 = (1.0 + (5.0f64).sqrt()) / 2.0;
    let res: u64 = (fi.powi(n as i32) / (5.0f64).sqrt() + 0.5).floor() as u64;

    BigUint::from(res)
}