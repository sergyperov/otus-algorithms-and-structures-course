use num_bigint::{BigUint};

/// Через рекурсию
pub fn calc(n: u64, cache: &mut Vec<BigUint>) -> BigUint {
    if n <= 1 {
        return BigUint::from(n);
    }

    if cache[n as usize] == BigUint::from(0u64) {
        cache[n as usize] = calc(n - 1, cache) + calc(n - 2, cache);
    }

    return cache[n as usize].clone();
}