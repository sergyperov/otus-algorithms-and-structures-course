use std::fs;
use std::env;
use std::io::Read;
use std::time::{Duration, Instant};
use termion::{color, style};

/*
Набор методов, позволяющих проводить простейшее тестирование вида f(in) == out
 */

fn local_dir(append: &str) -> std::io::Result<String> {
    let local_path = env::current_dir()?;
    let local_path_str = local_path.display();

    Ok(format!("{}{}", local_path_str, append))
}

struct TestsModule {
    path: String,
    test_counter: u32,
}

impl Iterator for TestsModule {
    type Item = (String, String);

    fn next(&mut self) -> Option<(String, String)> {
        let test_number = self.test_counter.to_string();
        let test_in_file = fs::File::open(format!("{}/test.{}.in", self.path, test_number));
        let test_out_file = fs::File::open(format!("{}/test.{}.out", self.path, test_number));

        if test_in_file.is_err() || test_out_file.is_err() {
            return None;
        }

        self.test_counter += 1;

        let mut test_in = String::new();
        let mut test_out = String::new();
        test_in_file.and_then(|mut f| f.read_to_string(&mut test_in));
        test_out_file.and_then(|mut f| f.read_to_string(&mut test_out));


        Some((test_in, test_out))
    }
}

fn test_module(path: String) -> TestsModule {
    TestsModule { path, test_counter: 0 }
}

pub fn run_tests(f: fn(String) -> String, tests_path: &str) -> std::io::Result<()> {
    let path = local_dir(tests_path)?;

    for (i, (test_in, test_out)) in test_module(path).enumerate() {
        let f_res: String = f(test_in);
        if f_res.trim() == test_out.trim() {
            println!("{}Test {} passed", color::Fg(color::Green), i.to_string());
        } else {
            println!("{}Test {} failed", color::Fg(color::Red), i.to_string());
            println!("Got: {}, expected: {}", f_res, test_out);
        }
    }

    Ok(())
}

pub fn run_time_tests(f: fn(String) -> String, tests_path: &str, count: u32) -> std::io::Result<()> {
    let path = local_dir(tests_path)?;

    for (i, (test_in, test_out)) in test_module(path).enumerate() {
        let mut total_duration_ms: u64 = 0;
        let mut total_duration_ns: u64 = 0;
        for _ in 0..count {
            let start = Instant::now();
            f(test_in.clone());
            total_duration_ms += start.elapsed().as_millis() as u64;;
            total_duration_ns += start.elapsed().as_nanos() as u64;
        }
        let (total_duration, unit) = if total_duration_ms > 0 {
            (total_duration_ms, "ms")
        } else {
            (total_duration_ns, "ns")
        };

        let time_per_test = (total_duration as f64) / (count as f64);

        println!("{} -> {}{}", test_in.trim().replace("\r\n", ", "), time_per_test, unit);
    }

    Ok(())
}
