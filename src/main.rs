mod testing;
mod tasks;

fn main() -> std::io::Result<()> {
    // testing::run_tests(tasks::strings::string_length, "/tasks_tests/0.String")?;
    // testing::run_tests(tasks::tickets::tickets, "/tasks_tests/1.Tickets")?;
    // tasks::harry_potter::create_squares()?;

    // testing::run_tests(tasks::gcd::euclid, "/tasks_tests/2.GCD")?;
    // testing::run_tests(tasks::gcd::euclid_mod, "/tasks_tests/2.GCD")?;
    // testing::run_tests(tasks::gcd::bits, "/tasks_tests/2.GCD")?;

    // testing::run_tests(tasks::power::iterative, "/tasks_tests/3.Power")?;
    // testing::run_tests(tasks::power::jumps, "/tasks_tests/3.Power")?;
    // testing::run_tests(tasks::power::optimized, "/tasks_tests/3.Power")?;

    // testing::run_tests(tasks::fibonacci::recursive, "/tasks_tests/4.Fibo")?;
    // testing::run_tests(tasks::fibonacci::iterative, "/tasks_tests/4.Fibo")?;
    // testing::run_tests(tasks::fibonacci::formula, "/tasks_tests/4.Fibo")?;
    // testing::run_tests(tasks::fibonacci::matrix, "/tasks_tests/4.Fibo")?;

    // testing::run_tests(tasks::primes::unoptimized, "/tasks_tests/5.Primes")?;
    // testing::run_tests(tasks::primes::optimized, "/tasks_tests/5.Primes")?;
    // testing::run_tests(tasks::primes::eratosthenes_sieve, "/tasks_tests/5.Primes")?;
    // testing::run_tests(tasks::primes::eratosthenes_sieve_bitwise, "/tasks_tests/5.Primes")?;
    // testing::run_tests(tasks::primes::eratosthenes_sieve_fast, "/tasks_tests/5.Primes")?;

    Ok(())
}
